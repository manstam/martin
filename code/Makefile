# Copyright (C) 2009-2020 Joachim Brod, Emmanuel Stamou
#
# This file is part of MaRTIn.
#
# MaRTIn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MaRTIn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MaRTIn.  If not, see <https://www.gnu.org/licenses/>.
#
# For further details see the AUTHORS file in the main MaRTIn directory.

############################################
### Configuration ##########################
############################################

# do not want make's own stuff
MAKEFLAGS=-r

# fix the shell
SHELL=/bin/sh

# QGRAF program to use (can be overwritten by martin.py)
QGRAF=qgraf

# FORM program to use (can be overwritten by martin.py)
FORM=form

# FORM options (can be overwritten by martin.py)
FORMOPT="-M"

# current GIT revision (passed on by martin.py)
GIT=""

# The path to the directory where the Makefile is
MAKE_DIR:=$(patsubst %/,%,$(dir $(firstword $(MAKEFILE_LIST))))

# paths specific to our 2loopForm directory tree
MAINFRM         =$(MAKE_DIR)/main.frm
SUMPROG         =$(MAKE_DIR)/sum.frm
QGRAF_FORM_STYLE=$(MAKE_DIR)/qgraf_styles/main.sty
QGRAF_GVIZ_STYLE=$(MAKE_DIR)/qgraf_styles/neato.sty
PRC_DIR         =$(MAKE_DIR)/prc
PRC_PROG        =$(wildcard $(PRC_DIR)/*)

##################################################
# The input to the Makefile
##################################################

# paths and variables that depend on the user and the loop.dat that
# must be computed. They MUST be passed to the makefile.

# A loop.dat can have a suffix loop.blafasel.dat in which case
# the PROBLEM_SUFFIX must be set to PROBLEM_SUFFIX=.blafasel
# If loop.dat should be computed then PROBLEM_SUFFIX=""?
ifndef PROBLEM_SUFFIX
  # strictly speaking this is not necessary but let's be pedantic
  PROBLEM_SUFFIX=""
endif

ifndef PROBLEM_DIRECTORY
  $(error You must specify the problem folder where the loop.dat is via PROBLEM_DIRECTORY="")
endif

ifndef RESULTS_DIRECTORY
  $(error You must specify a result folder via RESULTS_DIRECTORY="")
endif

ifndef USERPRC_DIRECTORIES
  # strictly speaking this is not necessary but let's be pedantic
  USERPRC_DIRECTORIES=""
endif

ifndef FMODELS
  $(error You must specify the FORM models via FMODELS="")
endif

ifndef QMODELS
  $(error You must specify the QGRAF models via QMODELS="")
endif

# remove possible trailing '/' that the user may have included
PROBLEM_DIR  = $(patsubst %/,%,$(PROBLEM_DIRECTORY))
RESULTS_DIR  = $(patsubst %/,%,$(RESULTS_DIRECTORY))
USERPRC_DIRS = $(foreach dir,$(USERPRC_DIRECTORIES),$(patsubst %/,%,$(dir)))

#############################

FMODEL_DIRS  =$(foreach model,$(FMODELS),$(patsubst %/,%,$(dir $(model))))
QMODEL_DIRS  =$(foreach model,$(QMODELS),$(patsubst %/,%,$(dir $(model))))
USERPRC_PROG =$(foreach dir,$(USERPRC_DIRS),$(wildcard $(dir)/*))

empty:=
space:= $(empty) $(empty)
USERPRC_DIRS_colon = $(subst $(space),:,$(USERPRC_DIRS))
FMODEL_DIRS_colon  = $(subst $(space),:,$(FMODEL_DIRS))

FORM_DIR =$(RESULTS_DIR)/form$(PROBLEM_SUFFIX)
MATH_DIR =$(RESULTS_DIR)/math$(PROBLEM_SUFFIX)
DIAG_DIR =$(RESULTS_DIR)/diagrams$(PROBLEM_SUFFIX)
INFO_DIR =$(RESULTS_DIR)/info$(PROBLEM_SUFFIX)

LDAT     =$(PROBLEM_DIR)/loop$(PROBLEM_SUFFIX).dat
FDAT     =$(RESULTS_DIR)/form$(PROBLEM_SUFFIX).dat
QDAT     =$(RESULTS_DIR)/qgraf$(PROBLEM_SUFFIX).dat
QLAG     =$(RESULTS_DIR)/qmodel$(PROBLEM_SUFFIX).lag
QLIST    =$(RESULTS_DIR)/qlist$(PROBLEM_SUFFIX).dat
MAKEINFO =$(RESULTS_DIR)/make$(PROBLEM_SUFFIX).info
GDOT     =$(RESULTS_DIR)/graphs$(PROBLEM_SUFFIX).dot
PSFILE   =$(RESULTS_DIR)/graphs$(PROBLEM_SUFFIX).ps
PDFFILE  =$(RESULTS_DIR)/graphs$(PROBLEM_SUFFIX).pdf
RDOT     =$(RESULTS_DIR)/rgraphs$(PROBLEM_SUFFIX).dot
RTEX     =$(RESULTS_DIR)/rgraphs$(PROBLEM_SUFFIX).tex
RPDFFILE =$(RESULTS_DIR)/rgraphs$(PROBLEM_SUFFIX).pdf

RDRAW_DIR=$(patsubst %/,%,$(dir $(RDRAW)))
QGRAF_RVIZ_STYLE=$(RDRAW_DIR)/richard_draw.sty
RDRAW_RESDIR =$(RESULTS_DIR)/rdraw$(PROBLEM_SUFFIX)
ifeq ($(RJSONS),)
  RJSONS_CMD=
else
  RJSONS_CMD=$(foreach json,$(RJSONS),--style-file $(json))
endif


################################################################
# compute dependencies from the loop.dat input file
################################################################

pound=\#
LOOP=$(shell awk 'BEGIN {active=0;} \
                  /\*--$(pound)\[ *QGRAF *:/ {active=1;} \
                  /^ *loops *= *[0-9]* *;/ {if (active) {sub(/^ *loops *= */, ""); sub(/ *; *$$/, ""); print;}}; \
                  /\*--$(pound)\] *QGRAF *:/ {active=0};' < $(LDAT))


################################################################
# the $(MAKEINFO) file must be included/generated
# (it contais the number of diagrams)
################################################################

ifeq ($(MAKECMDGOALS),)
  NO_INCLUDE=0
else ifeq ($(filter-out clean clean-dia clean-res clean-str clean-gra clean-sum,$(MAKECMDGOALS)),)
  NO_INCLUDE=1
else
  NO_INCLUDE=0
endif

ifeq ($(NO_INCLUDE),0)
  include $(MAKEINFO)
endif


################################################################
# The main targets, i.e., the form results
################################################################

DIAGRAMS   =$(wildcard $(DIAG_DIR)/*)
RESULTS    =$(addprefix $(FORM_DIR)/, $(addsuffix .sav, $(basename $(notdir $(DIAGRAMS)))))
RESULT_SUM =$(FORM_DIR)/diasum.sav


################################################################
# Calculate all diagrams
# This is the first (and default) target
################################################################

.PHONY: all
all: $(RESULTS)


# the rule to generate the qgraf.dat from the loop.dat
.PHONY: qdat
qdat: $(QDAT)
#.INTERMEDIATE: $(QDAT)
#$(RESULTS_DIR)/qgraf%.dat: $(PROBLEM_DIR)/loop%.dat
$(QDAT): $(LDAT)
	@echo "Generating $@ ..."
	@mkdir -p $(RESULTS_DIR)
	@awk 'BEGIN {active=0;} \
	     /\*--#\[ *QGRAF *:/ {active=1;} \
	     !/\*--#[\[\]] *QGRAF *:/ {if (active) {print; }}; \
             /\*--#\] *QGRAF *:/ {active=0};' < $< > $@.temp
	@if ! cmp -s $@.temp $@; then \
	        mv $@.temp $@; \
	fi
	@rm -f $@.temp


# the rule to generate the form.dat from the loop.dat
.PHONY: fdat
fdat: $(FDAT)
#.INTERMEDIATE: $(FDAT)
#$(RESULTS_DIR)/form%.dat: $(PROBLEM_DIR)/loop%.dat
$(FDAT): $(LDAT)
	@echo "Generating $@ ..."
	@mkdir -p $(RESULTS_DIR)
	@awk 'BEGIN {active=1;} \
	     /\*--#\[ *QGRAF *:/ {active=0;} \
             // {if (active) {print; }}; \
             /\*--#\] *QGRAF *:/ {active=1};' < $< > $@.temp
	@if ! cmp -s $@.temp $@; then \
	        mv $@.temp $@; \
	fi
	@rm -f $@.temp


# the rule to generate the combined QGRAF model
.PHONY: qlag
qlag: $(QLAG)
#.INTERMEDIATE: $(QLAG)
$(QLAG): $(QMODELS)
	@echo "Generating $@ ..."
	@mkdir -p $(RESULTS_DIR)
	@cat $^ > $@


# the rule to create an information file with the number of diagrams to be calculated
.PHONY: info
info: $(MAKEINFO)
#$(RESULTS_DIR)/make%.info: $(RESULTS_DIR)/qlist%.dat
$(MAKEINFO): $(QLIST)
	@echo "Generating $@ ..."
	@awk '/NUMBER_OF_DIAGRAMS/ { sub(/^ *NUMBER_OF_DIAGRAMS *= */, ""); \
		printf("NUMBER_OF_DIAGRAMS = %s\n", $$0); }' $< > $@ || { rm -rf $@ ; exit 1; }
	@if [ -n "$(GIT)" ]; then \
	   echo "GIT_COMMIT =" $(GIT) >> $@ ; \
	fi


# the function which runs qgraf on our specially prepared qgraf.dat files
# call like $(call run_qgraf,qgraf.dat,qgrafmodel.lag,stylefile,outfile,VERB(0 or 1))
define run_qgraf
  tempdir=$$(mktemp -d /tmp/tmp.2loop.XXXXXXXXXXXXXX); \
  cp $(2) $$tempdir/model.lag; \
  cp $(3) $$tempdir/; \
  echo "output = '$(notdir $(4))';" >  $$tempdir/qgraf.dat; \
  echo "style  = '$(notdir $(3))';" >> $$tempdir/qgraf.dat; \
  echo "model  = 'model.lag';"      >> $$tempdir/qgraf.dat; \
  awk '// {sub(/^ *model *=/, "* model ="); print;}' < $(1) >> $$tempdir/qgraf.dat; \
  (cd $$tempdir ; \
   $(QGRAF) > stdout_qgraf ;  \
   awk '/^ *error *: */ {print $$0 >> "stderr_qgraf";}' < stdout_qgraf; \
   num=$$(awk '/^ *total *= */ {sub(/^ *total *= */, ""); print $$1 ;}' < stdout_qgraf); \
   if [ -e stderr_qgraf ] ; then \
        echo "QGRAF encountered an error." ; \
        cat stdout_qgraf ; \
        exit 1 ; \
   elif [ "$$num" -eq 0 ] ; then \
        echo "QGRAF found 0 diagrams." ; \
        cat stdout_qgraf ; \
        exit 1 ; \
   else \
        if [ $(5) -eq 1 ] ; then \
           cat stdout_qgraf ; \
        fi; \
   fi ; \
  ) && \
  mkdir -p $(dir $@) && \
  mv $$tempdir/$(notdir $(4)) $(dir $(4)) && \
  rm -rf $$tempdir
endef

# the rule to generate the qlist and cut it into individual diagrams
.PHONY: qlist
qlist: $(QLIST)
.INTERMEDIATE: $(QLIST)
#$(RESULTS_DIR)/qlist%.dat: $(RESULTS_DIR)/qgraf%.dat $(QLAG) $(QGRAF_FORM_STYLE)
$(QLIST): $(QDAT) $(QLAG) $(QGRAF_FORM_STYLE)
	@echo "Generating $@ ..."
	@rm -rf $(DIAG_DIR)
	@rm -rf $(FORM_DIR)
	@rm -rf $(INFO_DIR)
	@rm -rf $(MATH_DIR)
	@rm -rf $(GDOT)
	@rm -rf $(PDFFILE)
	@rm -rf $(PSFILE)
	@echo "Running QGRAF ..."
	@VERB=1; $(call run_qgraf,$<,$(QLAG),$(QGRAF_FORM_STYLE),$@,$$VERB)
	@mkdir -p $(DIAG_DIR)
	@awk 'BEGIN {write=0; dia=0} \
             /\*--#\[ *dia[0-9]* *:/ {write=1; dia+=1;} \
	     // {if (write) {print $$0 > "$(DIAG_DIR)/dia"dia".dot";}} \
             /\*--#\] *dia[0-9]* *:/ {write=0;}' < $@;

# the rule to generate the .dot file for graphviz
.PHONY: gdot
gdot: $(GDOT)
.INTERMEDIATE: $(GDOT)
#$(RESULTS_DIR)/graphs%.dot: $(RESULTS_DIR)/qgraf%.dat $(QLAG) $(QGRAF_GVIZ_STYLE)
$(GDOT): $(QDAT) $(QLAG) $(QGRAF_GVIZ_STYLE)
	@echo "Generating $@ ..."
	@echo "Running QGRAF ..."
	@VERB=0; $(call run_qgraf,$<,$(QLAG),$(QGRAF_GVIZ_STYLE),$@,$$VERB)

# the rule to draw the diagrams with neato
.PHONY: pdf
pdf: $(PDFFILE)
$(RESULTS_DIR)/graphs%.pdf: $(RESULTS_DIR)/graphs%.dot
	@echo "Generating PDF file : $@ ..."
	@awk '// {sub(/color=\-/, "color=black"); print;}' < $< > $<.tmp && mv $<.tmp $<
	@awk '// {sub(/color=\+/, "color=red3");  print;}' < $< > $<.tmp && mv $<.tmp $<
	@cp $< $(basename $<)
	@neato -Tpdf -O $(basename $<)
	@rm -f $(basename $<)
	@mv $(basename $<).pdf $(basename $<).1.pdf
	@batch=1; \
	i=1; \
	j=1; \
	com=1000; \
	files=""; \
	bfiles=""; \
	while [ $$i -le $(NUMBER_OF_DIAGRAMS) ] ; do \
	    files="$$files $(basename $<).$$i.pdf" ; \
	    if [ $$j -lt $$com ] && [ $$i -ne $(NUMBER_OF_DIAGRAMS) ] ; then \
	        j=$$((j+1)) ; \
	    else \
	        j=1; \
		pdfunite $$files $(basename $<).batch.$$batch.pdf ; \
		bfiles="$$bfiles $(basename $<).batch.$$batch.pdf" ; \
		batch=$$((batch+1)); \
		rm $$files; \
		files=""; \
	    fi ; \
	    i=$$((i+1)) ; \
	done ; \
	pdfunite $$bfiles $@
	@#pdfjam -o $@ $$files >/dev/null 2>&1
	@rm $(basename $<).batch.[0-9]*.pdf

# the rule to generate the .dot file for richard_draw.py
.PHONY: rdot
rdot: $(RDOT)
.INTERMEDIATE: $(RDOT)
#$(RESULTS_DIR)/rgraphs%.dot: $(RESULTS_DIR)/qgraf%.dat $(QLAG) $(QGRAF_RVIZ_STYLE)
$(RDOT): $(QDAT) $(QLAG) $(QGRAF_RVIZ_STYLE)
	@echo "Generating $@ ..."
	@echo "Running QGRAF ..."
	@VERB=0; $(call run_qgraf,$<,$(QLAG),$(QGRAF_RVIZ_STYLE),$@,$$VERB)


# the rule to generate the .tex file with richard_draw.py
.PHONY: rtex
rtex: $(RTEX)
#$(RESULTS_DIR)/rgraphs%.tex: $(RESULTS_DIR)/rgraphs%.dot
$(RTEX): $(RDOT) $(RJSONS)
	@echo "Generating $@ ..."
	@echo "Running richard_drawer.py ..."
	$(RDRAW) $< --output-file $@ $(RJSONS_CMD)

# the rule to generate the .pdf file of richard_draw.py
.PHONY: rpdf
rpdf: $(RPDFFILE)
#$(RESULTS_DIR)/rgraphs%.pdf: $(RESULTS_DIR)/rgraphs%.tex
$(RPDFFILE): $(RTEX)
	@echo "Generating $@ ..."
	@echo "Running lualatex ..."
	@mkdir -p $(RDRAW_RESDIR)
	@lualatex --output-dir $(RDRAW_RESDIR) $<
	@mv $(RDRAW_RESDIR)/$(@F) $@
	@echo "Generated" $@

# the rule to make the tex file for a single diagram
.PRECIOUS: $(RDRAW_RESDIR)/rgraphs_dia%.tex
$(RDRAW_RESDIR)/rgraphs_dia%.tex: $(RDOT) $(RJSONS)
	@echo "Generating $@ ..."
	@mkdir -p $(RDRAW_RESDIR)
	@echo "Running richard_drawer.py ..."
	@$(RDRAW) $< --output-file $@ --dia-num $* --dia-size large $(RJSONS_CMD)

.PRECIOUS: $(RDRAW_RESDIR)/rgraphs_dia%.pdf
$(RDRAW_RESDIR)/rgraphs_dia%.pdf: $(RDRAW_RESDIR)/rgraphs_dia%.tex
	@echo "Generating $@ ..."
	@echo "Running lualatex ..."
	@lualatex --output-dir $(RDRAW_RESDIR) $<
	@echo "Generated" $@

# Shorthand for a single diagram
.PHONY: rtex%
rtex%: $(RDRAW_RESDIR)/rgraphs_dia%.tex
	@true
.PHONY: rpdf%
rpdf%: $(RDRAW_RESDIR)/rgraphs_dia%.pdf
	@true



# the rule to calculate one diagram and save it as a diaDDD.sav file (diaDDD.m is saved automatically)
$(FORM_DIR)/%.sav: $(DIAG_DIR)/%.dot $(FDAT) $(FMODELS) $(MAINFRM) $(PRC_PROG) $(USERPRC_PROG)
	@echo "Computing $@ ..."
	@mkdir -p $(FORM_DIR) $(MATH_DIR) $(INFO_DIR) && \
	export FORMPATH="$(PRC_DIR):$(USERPRC_DIRS_colon):$(FMODEL_DIRS_colon)" && \
	{ time $(FORM) $(FORMOPT) \
	        -D DIA=$(basename $(notdir $<)) \
		-D FILENAME=$< \
		-D PROBLEM=$(FDAT) \
		-D LOOP=$(LOOP) \
		-D FORMRESULTFILE=$(FORM_DIR)/$*.sav \
		-D MATHRESULTFILE=$(MATH_DIR)/$*.m \
		-D INFORESULTFILE=$(INFO_DIR)/$*.info \
		$(MAINFRM) ; } 2>> $(INFO_DIR)/$*.info
	@if [ -n "$(GIT)" ]; then \
	   echo ""                      >> $(INFO_DIR)/$*.info ; \
	   echo "GIT_COMMIT =" $(GIT) >> $(INFO_DIR)/$*.info ; \
	fi
	@echo "Done computing $@."

# Shorthand for a single diagram
dia%: $(FORM_DIR)/dia%.sav
	@true

# the rule to calculate the sum of all diagrams and save them as a diasum.sav file (diasum.m is saved automatically)
.PHONY: sum
sum: $(RESULT_SUM)
$(RESULT_SUM): $(RESULTS) $(SUMPROG)
	@echo "Computing $@ ..."
	@mkdir -p $(FORM_DIR) $(MATH_DIR) && \
	export FORMPATH="$(PRC_DIR):$(USERPRC_DIRS_colon)" && \
	$(FORM)	$(FORMOPT) \
		-D RESPATH=$(FORM_DIR) \
	        -D NUMDIA=$(NUMBER_OF_DIAGRAMS) \
		-D LOOP=$(LOOP) \
		-D FORMRESULTFILE=$@ \
		-D MATHRESULTFILE=$(MATH_DIR)/$(basename $(@F)).m \
		$(SUMPROG)

################################################################
# Clean
################################################################

.PHONY: clean clean-sum clean-dia clean-res clean-str clean-gra

clean:
	@echo "Cleaning ..."
	@rm -rf  $(DIAG_DIR)/dia[0-9]*
	@rm -rf  $(INFO_DIR)/dia[0-9]*\.info
	@rm -rf  $(FORM_DIR)/dia[0-9]*\.sav
	@rm -rf  $(MATH_DIR)/dia[0-9]*\.m
	@rm -rf  $(FORM_DIR)/diasum\.sav
	@rm -rf  $(MATH_DIR)/diasum\.m
	@rm -rvf $(DIAG_DIR)
	@rm -rvf $(INFO_DIR)
	@rm -rvf $(FORM_DIR)
	@rm -rvf $(MATH_DIR)
	@rm -rvf $(MAKEINFO)
	@rm -rvf $(QLIST)
	@rm -rvf $(FDAT)
	@rm -rvf $(QDAT)
	@rm -rvf $(QLAG)
	@rm -rvf $(GDOT)
	@rm -rvf $(RDRAW_RESDIR)
	@rm -rvf $(RDOT)
	@rm -rvf $(RTEX)
	@rm -rvf $(RPDFFILE)
	@rm -rvf $(PDFFILE)
	@rm -rvf $(PSFILE)
	@if [ -e $(RESULTS_DIR) ] ; then \
		rmdir --ignore-fail-on-non-empty $(RESULTS_DIR) ; \
	 fi
	@rm -rf  xform*.str /tmp/tmp.2loop.*

clean-sum:
	@echo "Clean sum ..."
	@rm -rvf $(RESULT_SUM)

clean-dia:
	@echo "Clean diagrams ..."
	@rm -rf $(DIAG_DIR)/dia[0-9]*
	@rm -rvf $(DIAG_DIR)
	@rm -rvf $(MAKEINFO)
	@rm -rvf $(QDAT)
	@rm -rvf $(QLIST)
	@rm -rvf $(QLAG)
	@rm -rvf $(GDOT)
	@rm -rvf $(PDFFILE)
	@rm -rvf $(PSFILE)
	@rm -rvf $(RDRAW_RESDIR)
	@rm -rvf $(RDOT)
	@rm -rvf $(RTEX)
	@rm -rvf $(RPDFFILE)
	@if [ -e $(RESULTS_DIR) ] ; then \
		rmdir --ignore-fail-on-non-empty $(RESULTS_DIR) ; \
	 fi
	@rm -rf  xform*.str /tmp/tmp.2loop.*

clean-res:
	@echo "Clean form and math results ..."
	@rm -rf $(INFO_DIR)/dia[0-9]*\.info
	@rm -rf $(FORM_DIR)/dia[0-9]*\.sav
	@rm -rf $(MATH_DIR)/dia[0-9]*\.m
	@rm -rf $(FORM_DIR)/diasum\.sav
	@rm -rf $(MATH_DIR)/diasum\.m
	@rm -rvf $(INFO_DIR)
	@rm -rvf $(FORM_DIR)
	@rm -rvf $(MATH_DIR)
	@if [ -e $(RESULTS_DIR) ] ; then \
		rmdir --ignore-fail-on-non-empty $(RESULTS_DIR) ; \
	 fi
	@rm -rf  xform*.str /tmp/tmp.2loop.*

clean-gra:
	@echo "Clean graphviz files ..."
	@rm -rvf $(GDOT)
	@rm -rvf $(PDFFILE)
	@rm -rvf $(PSFILE)
	@rm -rvf $(RDRAW_RESDIR)
	@rm -rvf $(RDOT)
	@rm -rvf $(RTEX)
	@rm -rvf $(RPDFFILE)
	@if [ -e $(RESULTS_DIR) ] ; then \
		rmdir --ignore-fail-on-non-empty $(RESULTS_DIR) ; \
	 fi
	@rm -rf  xform*.str /tmp/tmp.2loop.*

clean-str:
	@echo "Clean auxiliary form files ..."
	@if [ -e $(RESULTS_DIR) ] ; then \
		rmdir --ignore-fail-on-non-empty $(RESULTS_DIR) ; \
	 fi
	@rm -rf  xform*.str /tmp/tmp.2loop.*

################################################################
